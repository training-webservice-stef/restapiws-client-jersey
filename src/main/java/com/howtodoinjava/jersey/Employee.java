package com.howtodoinjava.jersey;

/**
 *
 * @author Stefanus
 */
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "employee")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee {

    private Integer id;
    private String employee_name, name;
    private Integer employee_salary, salary;
    private int employee_age, age;
    private String profile_image;

    public Employee() {

    }

    public Employee(Integer id, String employee_name) {
        this.id = id;
        this.employee_name = employee_name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public Integer getEmployee_salary() {
        return employee_salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    public int getEmployee_age() {
        return employee_age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", name=" + employee_name + "]";
    }
}
