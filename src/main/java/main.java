
import com.howtodoinjava.jersey.Employee;
import com.howtodoinjava.jersey.Employees;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.filter.LoggingFilter;

/**
 *
 * @author Stefanus
 */
public class main {

    public static void main(String[] args) throws MalformedURLException, IOException {
        getAllEmployee();
        addNewEmployee();
        deleteEmployee();
        updateEmployee();
        getEmployeeById(); // masih bug error!
    }

    public static void getAllEmployee() {

        Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
        WebTarget webTarget = client.target("http://dummy.restapiexample.com/api/v1").path("employees");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.get();

        Employees employees = response.readEntity(Employees.class);
        List<Employee> listOfEmployees = employees.getEmployeeList();

        System.out.println(response.getStatus());
        System.out.println(Arrays.toString(listOfEmployees.toArray(new Employee[listOfEmployees.size()])));
    }

    public static void addNewEmployee() {

        Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
        WebTarget webTarget = client.target("http://dummy.restapiexample.com/api/v1").path("create");

        Employee emp = new Employee();
        emp.setName("Stefanus");
        emp.setSalary(105000);
        emp.setAge(22);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = invocationBuilder.post(Entity.entity(emp, MediaType.APPLICATION_JSON));

        System.out.println(response.getStatus());
        System.out.println(response.readEntity(String.class));
    }

    public static void deleteEmployee() {

        Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
        WebTarget webTarget = client.target("http://dummy.restapiexample.com/api/v1").path("delete").path("10");

        Invocation.Builder invocationBuilder = webTarget.request();
        Response response = invocationBuilder.delete();

        System.out.println(response.getStatus());
        System.out.println(response.readEntity(String.class));
    }

    public static void updateEmployee() {

        Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
        WebTarget webTarget = client.target("http://dummy.restapiexample.com/api/v1").path("update").path("5");

        Employee emp = new Employee();
        emp.setName("Stefanus Update");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        Response response = invocationBuilder.put(Entity.entity(emp, MediaType.APPLICATION_XML));

        Employee employee = response.readEntity(Employee.class);

        System.out.println(response.getStatus());
        System.out.println(employee);
    }

    public static void getEmployeeById() {

        Client client = ClientBuilder.newClient(new ClientConfig().register(LoggingFilter.class));
        WebTarget webTarget = client.target("http://dummy.restapiexample.com/api/v1").path("employee").path("3");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        Response response = invocationBuilder.get();

        Employee employee = response.readEntity(Employee.class);

        System.out.println(response.getStatus());
        System.out.println(employee);
    }

}
